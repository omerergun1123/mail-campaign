from django.conf.urls import url, include
from .views import CampaignView, ContactView, check_uuid
from rest_framework import routers

router = routers.DefaultRouter(trailing_slash=True)
router.register(r'campaigns', CampaignView, basename='campaigns')
router.register(r'contacts', ContactView, basename='contacts')

urlpatterns = [
    url(r'', include(router.urls)),
    url(r'check-uuid/', check_uuid)
]
