from .models import Campaign, Contact
from .serializers import CampaignSerializer, ContactSerializer, CampaignSerializerMultipleObjectsLessData
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
import datetime
import uuid
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, renderer_classes
import json
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.core.exceptions import ValidationError, ObjectDoesNotExist

def check_email(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False

class ContactView(viewsets.ModelViewSet):
    def get_queryset(self):
        return Contact.objects.all()
    serializer_class = ContactSerializer

class CampaignView(viewsets.ModelViewSet):
    def get_queryset(self):
        return Campaign.objects.all()

    #Important point
    #Server response details of campaign only for id based requests like /campaigns/5/
    #Details are excluded for multiple campaign object request to get better performance
    def get_serializer_class(self):
        if len(self.kwargs)!=0:
            return CampaignSerializer
        return CampaignSerializerMultipleObjectsLessData

    @action(methods=['post'],detail=True,url_path='upload-contact-list',url_name='upload-contact-list')
    def upload_contact_list(self,request,pk=True):
        db_campaign = Campaign.objects.get(id = pk)
        uploaded_file = request.FILES['uploadedTextFile']
        file_content = uploaded_file.read().decode()
        lines = file_content.split('\n')
        contact_list = [] #empty list to fill with contact objects for final bulk object creation with one query
        pastContacts = Contact.objects.filter(campaign_id = db_campaign)
        for contact in lines:
            if contact and contact.strip():
                credentials = contact.split() #splits on whitespace regardless of whitespace length
                if(len(credentials) < 3):
                    return Response(data='Invalid line on txt file; missing name, lastname or email!', status=status.HTTP_400_BAD_REQUEST)
                last_block = credentials[len(credentials)-1]
                email = last_block[1:-1]
                if(last_block[0] != '<' or last_block[-1] != '>' or not check_email(email)):
                    return Response(data='Invalid email address block on contact list!', status=status.HTTP_400_BAD_REQUEST)
                if(pastContacts.filter(campaign_id = db_campaign, email = email).count()==0):
                    last_name = credentials[len(credentials)-2]
                    #remove whitespaces on line start if exists by .strip() method
                    first_name = contact[0:(len(contact) - len(email) - len(last_name) - 4)].strip()
                    contact_list.append(Contact(first_name = first_name, last_name = last_name, email = email, campaign_id = db_campaign))
        Contact.objects.bulk_create(contact_list)
        return Response(data='Contacts are created!', status=status.HTTP_202_ACCEPTED)

    @action(methods=['post'],detail=True,url_path='add-to-campaign',url_name='add-to-campaign')
    def add_to_campaign(self,request,pk=True):
        db_campaign = Campaign.objects.get(id = pk)
        contactList = request.data['contactList']
        #id__in operation will automatically ignore invalid contact id's on contact list
        Contact.objects.filter(campaign_id = db_campaign, id__in=contactList).update(included_on_campaign = True)
        return Response(data='Given contacts are included in campaign!', status=status.HTTP_202_ACCEPTED)
    
    @action(methods=['post'],detail=True,url_path='remove-from-campaign',url_name='remove-from-campaign')
    def remove_from_campaign(self,request,pk=True):
        db_campaign = Campaign.objects.get(id = pk)
        contactList = request.data['contactList']
        #id__in operation will automatically ignore invalid contact id's on contact list
        Contact.objects.filter(campaign_id = db_campaign, id__in=contactList).update(included_on_campaign = False)
        return Response(data='Given contacts are removed from campaign!', status=status.HTTP_202_ACCEPTED)
    
    @action(methods=['post'],detail=True,url_path='clear-contacts',url_name='clear-contacts')
    def clear_contacts(self,request,pk=True):
        db_campaign = Campaign.objects.get(id = pk)
        Contact.objects.filter(campaign_id = db_campaign).delete()
        return Response(data='All contacts are removed from campaign!', status=status.HTTP_202_ACCEPTED)

    @action(methods=['post'],detail=True,url_path='send-mail',url_name='send-mail')
    def send_mail(self,request,pk=True):
        db_campaign = Campaign.objects.get(id = pk)
        mailBody = request.data['mailBodyToSend']
        #accept only mail body in string format and reject empty string
        #if(not isinstance(mailBody,str) or not mailBody.strip()):
            #return Response(data='Email body parameter should be a valid non-empty string!', status=status.HTTP_400_BAD_REQUEST)
        #update campaign email body as well with the email body data in request
        db_campaign.email_body = mailBody
        db_campaign.save()
        #get contacts which are included in campaign and email is not sent
        contacts = Contact.objects.filter(campaign_id = db_campaign, email_sent = False, included_on_campaign = True)
        for contact in contacts:
            uuidForMailLink = str(uuid.uuid1())
            mailBodyFinal = mailBody + "\n\nTest link: " + "http://localhost:3000/mailredirect/" + uuidForMailLink
            send_mail('Mail Campaign Case Study Test Mail', mailBodyFinal, 'mailcampaigncasestudy@gmail.com', [contact.email], fail_silently=False)
            contact.email_sent = True
            contact.email_sent_time = datetime.datetime.now()
            contact.link_uuid = uuidForMailLink
            contact.save()
        return Response(data='Email is sent to selected contacts who did not get email before!', status=status.HTTP_202_ACCEPTED)
    

@api_view(["POST"])
@csrf_exempt
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def check_uuid(request):
    uuid = request.data['uuid']
    contacts = Contact.objects.filter(link_uuid = uuid)
    if(contacts.count() == 0):
        return Response(data='Invalid uuid!', status=status.HTTP_400_BAD_REQUEST)
    contacts.update(link_opened=True, link_opened_time=datetime.datetime.now())
    return Response(data='User clicked link info saved!', status=status.HTTP_202_ACCEPTED)