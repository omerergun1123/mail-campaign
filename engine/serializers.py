from django.db import reset_queries
from .models import Campaign, Contact
from rest_framework import serializers
from django.db.models import DurationField, F, ExpressionWrapper, DateTimeField
from datetime import datetime, timedelta
import time

class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all__'
    
class CampaignSerializerMultipleObjectsLessData(serializers.ModelSerializer):
    class Meta:
        model = Campaign
        fields = '__all__'

def duration_string(duration):
    # Removing the milliseconds of the duration field
    days = duration.days
    seconds = duration.seconds
    microseconds = duration.microseconds

    minutes = seconds // 60
    seconds = seconds % 60

    hours = minutes // 60
    minutes = minutes % 60

    string = '{:02d}:{:02d}:{:02d}'.format(hours, minutes, seconds)
    if days:
        string = '{} '.format(days) + string
    # if microseconds:
    #     string += '.{:06d}'.format(microseconds)

    return string

class CustomDurationField(DurationField):
    def prepare_value(self, value):
        if isinstance(value, datetime.timedelta):
            return duration_string(value)
        return value

class CampaignSerializer(serializers.ModelSerializer):
    contacts= serializers.SerializerMethodField()

    def get_contacts(self, instance):
        #annotate expression to reach custom field with query which is time difference between
        #email recieve and click the link, ExpressionWrapper works if only both email_sent_time and
        #link_opened_time is not None. Results of two query is concated due to this limitation
        campaign_contacts_link_opened = Contact.objects.annotate(
            time_diff_seconds=ExpressionWrapper(F('link_opened_time') - F('email_sent_time'), output_field=CustomDurationField())
        ).filter(campaign_id=instance, link_opened_time__isnull = False, email_sent_time__isnull = False)
        campaign_contacts_others = Contact.objects.filter(campaign_id=instance).exclude(id__in=campaign_contacts_link_opened.values('id'))
        return list(campaign_contacts_link_opened.values()) + list(campaign_contacts_others.values())

    class Meta:
        model = Campaign
        fields = ('id', 'name', 'email_body', 'contacts')