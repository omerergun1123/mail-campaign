# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

class Campaign(models.Model):
    name = models.CharField(max_length=32)
    email_body = models.TextField(null=True)

class Contact(models.Model):
    campaign_id = models.ForeignKey(Campaign, on_delete=models.CASCADE, related_name='contactCampaignId')
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    email = models.CharField(max_length=1024)
    email_sent = models.BooleanField(default=False)
    link_opened = models.BooleanField(default=False)
    email_sent_time = models.DateTimeField(null=True)
    link_opened_time = models.DateTimeField(null=True)
    included_on_campaign = models.BooleanField(default=False)
    link_uuid = models.CharField(max_length=1024)