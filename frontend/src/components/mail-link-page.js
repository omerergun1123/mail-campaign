import React , {Component} from "react";
import 'antd/dist/antd.css';
import {
    Spin
} from 'antd';
import {checkUuidAsync} from "../api-caller"

class MailLinkPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            linkVerified: null
        }
        this.checkUuid = this.checkUuid.bind(this)
        this.checkUuid()
    }

    async checkUuid() {
        checkUuidAsync().then(response => {
            this.setState({
                linkVerified: true
            })
        }).catch(error => {
            this.setState({
                linkVerified: false
            })
        })
    }

    render () {  
        if(this.state.linkVerified == null){
            return (
                <Spin size="large" style={{width:'25%', marginLeft:'37%', marginTop: '5%'}}/>
            )
        }
        else if(this.state.linkVerified){
            return (
                <h1 style={{width:'25%', marginLeft:'37%', marginTop: '5%'}}>Successful mail link!</h1>
            )
        }else{
            return (
                <h1 style={{width:'25%', marginLeft:'37%', marginTop: '5%'}}>Invalid mail link!</h1>
            )
        }
    }
}

export default MailLinkPage