import React , {Component} from "react";
import 'antd/dist/antd.css';
import {
    Button,
    Upload,
    Modal,
    Icon,
    Table,
    Checkbox,
    Input,
    Spin
} from 'antd';
import { Row, Col } from 'antd/lib/grid';
import {getCampaignsAsync, handleContactUploadAsync, addToCampaignAsync, removeFromCampaignAsync, clearContactsAsync,
    sendMailAsync, createCampaignAsync, deleteCampaignAsync, deleteContactAsync, getContactsAsync} from '../api-caller'
const { TextArea } = Input;

class CampaingListPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            campaigns: [],
            uploadContactList: [],
            emailSendList:[],
            mailContent: "",
            visible:false,
            newCampaignName: "",
            newCampaignEmail: ""
        }
        this.getCampaigns = this.getCampaigns.bind(this);
        this.handleContactUpload = this.handleContactUpload.bind(this);
        this.addToCampaign = this.addToCampaign.bind(this);
        this.removeFromCampaign = this.removeFromCampaign.bind(this)
        this.sendMail = this.sendMail.bind(this)
        this.createCampaign = this.createCampaign.bind(this)
        this.getCampaigns()
    }
    
    getCampaigns() {
        getCampaignsAsync()
        .then(response => {
            this.setState({
                campaigns: response.data,
                uploadContactList: [],
                visible: false,
                newCampaignName: "",
                newCampaignEmail: ""
            })
        });
    }

    handleContactUpload(campaignId) {
        var formData = new FormData();
        formData.append("uploadedTextFile", this.state.uploadContactList[0]);
        handleContactUploadAsync(campaignId, formData).then(response => {
            this.getContacts(campaignId)
        });
    }

    addToCampaign(campaignId, contactId) {
        addToCampaignAsync(campaignId, contactId).then(response => {
            this.getContacts(campaignId)
        });
    }

    removeFromCampaign(campaignId, contactId) {
        removeFromCampaignAsync(campaignId, contactId).then(response => {
            this.getContacts(campaignId)
        });
    }

    sendMail(campaignId) {
        Modal.info({
            title: 'Emails are being sent!',
            content: <Spin style={{marginLeft:'30%', marginTop:'5%'}} size='large'/>,
            okButtonProps: { style: { display: 'none' } }
        });
        sendMailAsync(this.state.mailContent, campaignId).then(response => {
            Modal.destroyAll()
            this.getContacts(campaignId)
        });
    }

    createCampaign() {
        var formData = new FormData();
        formData.append("name", this.state.newCampaignName);
        formData.append("email_body", this.state.newCampaignEmail)
        createCampaignAsync(formData).then(response => {
            this.getCampaigns()
        });
    }

    deleteCampaign(campaignId){
        deleteCampaignAsync(campaignId).then(response => {
            this.getCampaigns()
        });
    }

    deleteContact(campaignId, contactId){
        deleteContactAsync(contactId).then(response => {
            this.getContacts(campaignId)
        });
    }

    clearContacts(campaignId){
        clearContactsAsync(campaignId).then(response => {
            this.getContacts(campaignId)
        });
    }

    getContacts(campaignId) {
        getContactsAsync(campaignId).then(response => {
            let campaignlist = this.state.campaigns
            let detailedCampaignIndex = this.state.campaigns.findIndex(object => object.id == campaignId)
            campaignlist[detailedCampaignIndex] = response.data
            this.setState({
                campaigns: campaignlist,
                mailContent: response.data.email_body
            })
        });
    }

    renderModal() {
        return (
            <Modal
                title="Create New Campaign"
                visible={this.state.visible}
                onOk={() => this.createCampaign()}
                onCancel={() => {
                    Modal.destroyAll()
                    this.setState({visible:false})
                }}
                destroyOnClose={true}
                >
                <h4>Campaign Name</h4>
                <TextArea
                    placeholder='Campaign Name'
                    onChange={(e) => this.setState({ newCampaignName: e.target.value })}
                />
                <br></br><br></br>
                <TextArea
                    placeholder='Campaign Email'
                    onChange={(e) => this.setState({ newCampaignEmail: e.target.value })}
                />
            </Modal>
        )
    }
    
    renderFileUpload() {
        return (
            <Row style = {{marginTop:'5%', marginLeft: '18%'}}>
                <Upload multiple={false}
                    accept="text/*"
                    listType='text'
                    fileList={this.state.uploadContactList}
                    beforeUpload={(_: RcFile, newLocalFiles: RcFile[]): boolean => {
                        if(!newLocalFiles[0].type.includes("text")){
                            Modal.error({
                                title: 'Error!',
                                content: 'Only txt files can be uploaded!',
                            });
                            return false
                        }
                        this.setState({
                            uploadContactList: newLocalFiles
                        });
                        return false;
                    }}
                    onRemove={(): boolean => {
                        this.setState({ 
                            uploadContactList: []
                        });
                        return false;
                    }}
                >
                    <Button>
                        <Icon type="upload" /> Upload Contact List
                    </Button>
                </Upload>
            </Row>
        )
    }
    
    
    secondsToHms (seconds) {
        const days = Math.floor(seconds / 86400);
        const remainderSeconds = seconds % 86400;
        const hms = new Date(remainderSeconds * 1000).toISOString().substring(11, 19);
        return hms.replace(/^(\d+)/, h => `${Number(h) + days * 24}`.padStart(2, '0'));
    };

    handleExpand(record) {
        let campaignId = record.id
        const columns = [
            {
                title: 'First Name',
                dataIndex: 'first_name',
                key: 'first_name',
            },
            {
                title: 'Last Name',
                dataIndex: 'last_name',
                key: 'last_name',
            },
            {
                title: 'Email',
                dataIndex: 'email',
                key: 'email',
            },
            {
                title: 'Include In Campaign',
                dataIndex: 'id',
                key: 'id',
                render: (id, record) => {   
                    return(
                        <Checkbox checked = {record.included_on_campaign} onChange={(value) => {
                            if(value.target.checked)
                                this.addToCampaign(campaignId,id)
                            else{
                                this.removeFromCampaign(campaignId, id)
                            }
                        }}></Checkbox>
                    )
                },
            },
            {
                title: 'Email Sent',
                dataIndex: 'email_sent',
                key: 'email_sent',
                render: (email_sent) => {   
                    return(
                        email_sent ? <p>Yes</p> : <p>No</p>
                    )
                },
            },
            {
                title: 'Clicked Link',
                dataIndex: 'link_opened',
                key: 'link_opened',
                render: (link_opened) => {   
                    return(
                        link_opened ? <p>Yes</p> : <p>No</p>
                    )
                },
            },
            {
                title: 'Delete',
                dataIndex: 'id',
                key: 'id',
                render: (id) => {
                    return(
                        <Button onClick={() => this.deleteContact(campaignId,id)}>Delete</Button>
                    )
                }
            },
            {
                title: 'Time Diff(hh:mm:ss)',
                dataIndex: 'id',
                key: 'id',
                render: (id, record) => {
                    return(
                        (record.time_diff_seconds) && <p>{this.secondsToHms(record.time_diff_seconds)}</p>
                    )
                }
            },
        ];
        return (
            <div>
                <Row>
                    <Col span ={6}>
                        {this.renderFileUpload()}
                    </Col>
                    <Col span ={5} style={{marginTop:'1.2%'}}>
                        <Button disabled={this.state.uploadContactList.length==0} onClick={() => this.handleContactUpload(campaignId)}>
                            Upload
                        </Button>
                    </Col>
                </Row>
                <Row style={{marginTop:'1.2%'}}>
                    <Col span ={14}>
                        <TextArea
                            rows={4}
                            maxLength={4000}
                            defaultValue={record.email_body}
                            placeholder='Enter email body here'
                            onChange={(value) => this.setState({mailContent: value.target.value})}
                        />
                    </Col>
                    <Col span ={4} style={{marginTop:'1.2%', marginLeft:'2%'}}>
                        <Button onClick={() => this.sendMail(campaignId)}>
                            Send Mail
                        </Button>
                    </Col>
                    <Col span ={4} style={{marginTop:'1.2%', marginLeft:'2%'}}>
                        <Button onClick={() => this.clearContacts(campaignId)}>
                            Clear Contacts
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col span ={22}>
                        <Table
                            dataSource={record.contacts}
                            scroll={{ y: 500 }}
                            columns={columns}
                        >
                        </Table>
                    </Col>
                </Row>
            </div> 
        );
    }

    renderCampaignsTable(){
        const columns = [
            {
                title: 'Id',
                dataIndex: 'id',
                key: 'id',
            },
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                render: text => <a>{text}</a>,
            },
            {
                title: 'Cancel',
                dataIndex: 'id',
                key: 'id',
                render: (id) => {
                    return(
                        <Button onClick={() => this.deleteCampaign(id)}>Cancel</Button>
                    )
                }
            },
        ];
        return(
            <Table
                dataSource={this.state.campaigns}
                scroll={{ y: 800 }}
                columns={columns}
                expandedRowRender={(record) => <>{this.handleExpand(record)} </>}
                onExpand={(record,key) => {this.getContacts(key.id)}}
            >
            </Table>
        )
    }

    render () {  
        return (
            <div style ={{width:'90%', marginLeft: '5%', marginTop: '5%'}}>
                <Row>
                    <Button onClick={() => this.setState({visible:true})}>Create New Campaign</Button>
                </Row> 
                <Row style={{marginTop:'2%'}}>
                    <Col span = {24}>
                        {this.renderModal()}
                        {this.renderCampaignsTable()}
                    </Col>
                </Row> 
            </div>
        )
    }
}

export default CampaingListPage