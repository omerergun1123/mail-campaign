import './App.css';
import {BrowserRouter as Router,Route,Switch,Link,Redirect} from "react-router-dom";
import CampaingListPage from './components/campaign-list'
import MailLinkPage from './components/mail-link-page'

function App() {
  return (
    <Router>
        <Switch>
            <Route exact path= "/mailredirect/:uidb64" component={MailLinkPage}  />
            <Route exact path= "/campaignlist" component={CampaingListPage}  />
            <Redirect push to='/campaignlist' />
        </Switch>
    </Router> 
)
}

export default App;
