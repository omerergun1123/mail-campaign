import axios from 'axios';
const urlBase = 'http://localhost:8000'

export async function checkUuidAsync () {
    const url = window.location.href.split('/')
    const uuid = url.pop()
    let response = await axios.post(`${urlBase}/engine/check-uuid/`,{
        uuid: uuid,
    })
    return response
}

export async function getCampaignsAsync () {
    let response = await axios.get(`${urlBase}/engine/campaigns/`, )
    return response
}

export async function handleContactUploadAsync (campaignId, formData) {
    let response = await axios.post(`${urlBase}/engine/campaigns/${campaignId}/upload-contact-list/`,formData)
    return response
}

export async function addToCampaignAsync (campaignId, contactId) {
    let response = await axios.post(`${urlBase}/engine/campaigns/${campaignId}/add-to-campaign/`,{
        contactList: [contactId],
    })
    return response
}

export async function removeFromCampaignAsync (campaignId, contactId) {
    let response = await axios.post(`${urlBase}/engine/campaigns/${campaignId}/remove-from-campaign/`,{
        contactList: [contactId],
    })
    return response
}

export async function sendMailAsync (mailBodyToSend, campaignId) {
    let response = await axios.post(`${urlBase}/engine/campaigns/${campaignId}/send-mail/`,
        {
            mailBodyToSend: mailBodyToSend,
        }
    )
    return response
}

export async function createCampaignAsync (formData) {
    let response = await axios.post(`${urlBase}/engine/campaigns/`, formData)
    return response
}

export async function deleteCampaignAsync (campaignId) {
    let response = await axios.delete(`${urlBase}/engine/campaigns/${campaignId}/`)
    return response
}

export async function deleteContactAsync (contactId) {
    let response = await axios.delete(`${urlBase}/engine/contacts/${contactId}/`)
    return response
}

export async function clearContactsAsync (campaignId) {
    let response = await axios.post(`${urlBase}/engine/campaigns/${campaignId}/clear-contacts/`)
    return response
}

export async function getContactsAsync (campaignId) {
    let response = await axios.get(`${urlBase}/engine/campaigns/${campaignId}/`)
    return response
}