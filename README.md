## Demo Video With Testing Frontend

![demo](/uploads/377db281d4e1c005d749cb6982ad1e79/demo.mp4)

Mail Campaign Web Service that uses:

- Django for backend,
- PostgreSQL for database,
- React for the frontend(only for the show and test, all operations are done on system backend as expected)
- Docker, docker-compose for deployment

## Deploy App With Docker-Compose

```
git clone https://gitlab.com/omerergun1123/mail-campaign.git
cd mail-campaign
```
```
sudo docker-compose build
sudo docker-compose up -d
sudo docker-compose run mail_campaign_web python manage.py migrate
```

Backend Service starts to work on http://localhost:8000 and frontend application for the test works on http://localhost:3000

Also it is deployed on AWS machine:
You can reach:
- Its test UI from http://3.133.151.182:3000/
- Its backend from http://3.133.151.182:8000/ to test API calls

Probably reviewing requests at Chrome Dev Console Network panel by the usage of system test UI will be much better than my explanations.

## Endpoints

## Create Campaign and List Campaigns

```
- POST /engine/campaigns/ (waits "name" and "email_body" in form-data, creates new campaign with given credentials)

```

```
- GET /engine/campaigns/ (returns all the campaigns with campaign name and campaign email body)
```

![campaigns](/uploads/ff81f19a4f5bd0f60705793ded214a20/campaigns.png)

## Upload Text File(containing list of names and emails) To Campaign(add contacts multiple times)

```
- POST /engine/campaigns/{campaign_id}/upload-contact-list/ (waits "uploadedTextFile" as file in form-data, creates contacts for given campaign)
```

System supports txt file upload to the same campaign multiple times to add contacts.

## List of Contacts For Specified Campaign With Details

```
- GET /engine/campaigns/{campaign_id}/ (returns specified campain with its related contacts, relation is done by the usage of django serializer custom fields)
```

Contacts object includes following details for each contact:
- Included in campaign or not
- First name, last name, email
- Email is sent, true or false
- Link is clicked, true or false
- Email sent time 
- Link click time
- If email is sent and link is opened, "how long it took between the time to receive the email and click the link", "time_diff_seconds" parameter in response as seconds

```
- GET /engine/contacts/{contact_id}/ (also returns these json response) 
```

Important note: Server response details of campaign only for id based requests, details are excluded for multiple campaign objects request to get better performance.

![campaign1](/uploads/6c3507e0d9f5d1236dcc2efadb7b0704/campaign1.png)

## Select Contacts to Include in Campaign and Send Email

```
- POST /engine/campaigns/{campaign_id}/add-to-campaign/ (waits "contactList" as list of contact id's in request payload, includes these contacts in to campaign)
```
```
- POST /engine/campaigns/{campaign_id}/remove-from-campaign/ (waits "contactList" as list of contact id's in request payload, removes these contacts from campaign)
```
```
- POST /engine/campaigns/{campaign_id}/send-mail/ (waits "mailBodyToSend" as string in request payload, send emails to selected/included contacts of campaign who did not get email before)
```

Emails are sent from temporarily created "mailcampaigncasestudy@gmail.com" adress and can be recoginized as Spam so check your spam folder if you cannot see the test email. 

## Email Link Click and UUID Check

```
- POST /engine/check-uuid/ (waits "uuid" on as string on request payload, if uuid is valid it sets the link open time for the user)
```

## Other Service Capabilities(delete contact, clear contacts, delete campaign)

```
- DELETE /engine/contacts/{contact_id}/ (removes contact with given id from system)
- DELETE /engine/campaigns/{campaign_id}/ (removes campaign with given id from system)
- POST /engine/campaigns/{campaign_id}/clear-contacts/ (removes all the contacts related to the given campaign)
- GET /engine/contacts/ (returns all contacts in json format) 

```
## Survive a Reboot Requirement

"restart:always" parameter for each frontend, backend and postgresql containers will handle this.


